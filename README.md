# README #

Apps used to compare push and pull based streaming-overhead.

## Setup
```
./waf configure
./waf
sudo ./waf install
```

## Execution
```
./pull-producer
Programm Options:
  -i [ --own-id ] arg         Id-number of the instance (Required)
  -p [ --prefix ] arg         Prefix the PullProducer listens too. (Required)
  -s [ --data-size ] arg      The size of the datapacket in bytes. (Required)
  -f [ --freshness-time ] arg Freshness time of the content in seconds. 
                              (Default 5min)
```

```
./pull-consumer
Program options:
  -i [ --own-id ] arg   Id-number of the instance (Required)
  -p [ --prefix ] arg   Prefix the PullConsumer uses to request content. 
                        (Required)
  -r [ --rate ] arg     Interests per second issued. (Required)
  -l [ --lifetime ] arg Interest Lifetime (Default 1000msec)
```

```
./push-producer
Program options:
  -i [ --own-id ] arg                Id-number of the instance (Required)
  -p [ --prefix ] arg                Prefix the push-producer listens too.
                                     (Required)
  -s [ --data-size ] arg             The size of a data-packet in bytes.
                                     (Required)
  -r [ --rate ] arg                  The number of pushed data-packets per
                                     second (Required)
  -t [ --freshness-time ] arg (=300) Freshness time of the content in seconds.
                                     (Default 5min)
```

```
./push-consumer
Program options:
  -i [ --own-id ] arg           Id-number of the instance (Required)
  -p [ --prefix ] arg           Prefix the push-consumer uses to request
                                content (Required)
  -r [ --refresh-interval ] arg Interval of interest reissuing (Required, in
                                msec)
  -l [ --lifetime ] arg         Interest lifetime (Default 1000msec)
```

```
./pull-producer --own-id 1 --prefix /voice/1 --data-size 80
./pull-consumer --own-id 1 -p /voice/1 -r 100

./push-producer --own-id 1 --prefix /voice/1 --data-size 80 --rate 100
./push-consumer --own-id 1 --prefix /voice/1 --refresh-interval 2000 --lifetime 5000
```
```
timeout 10s ./pull-consumer --own-id 1 -p /voice/1 -r 1
```

## Examples of logfiles
```
cat S1.log (Server/Producer)
Time	Node	Type	Send/Receive	Name
1515765050563	S	I	R	/voice/1/0
1515765050570	S	D	S	/voice/1/0
1515765051563	S	I	R	/voice/1/1
1515765051569	S	D	S	/voice/1/1
1515765052563	S	I	R	/voice/1/2
1515765052568	S	D	S	/voice/1/2
1515765053563	S	I	R	/voice/1/3
1515765053569	S	D	S	/voice/1/3
1515765054563	S	I	R	/voice/1/4
1515765054569	S	D	S	/voice/1/4
1515765055563	S	I	R	/voice/1/5
1515765055568	S	D	S	/voice/1/5
1515765056563	S	I	R	/voice/1/6
1515765056568	S	D	S	/voice/1/6
1515765057563	S	I	R	/voice/1/7
1515765057569	S	D	S	/voice/1/7
1515765058563	S	I	R	/voice/1/8
1515765058568	S	D	S	/voice/1/8
```


```
cat C1.log (Client/Consumer)
Time	Node	Type	Send/Receive	Name
1515765050563	C	I	S	/voice/1/0
1515765050570	C	D	R	/voice/1/0
1515765051563	C	I	S	/voice/1/1
1515765051569	C	D	R	/voice/1/1
1515765052563	C	I	S	/voice/1/2
1515765052569	C	D	R	/voice/1/2
1515765053563	C	I	S	/voice/1/3
1515765053569	C	D	R	/voice/1/3
1515765054563	C	I	S	/voice/1/4
1515765054569	C	D	R	/voice/1/4
1515765055563	C	I	S	/voice/1/5
1515765055568	C	D	R	/voice/1/5
1515765056563	C	I	S	/voice/1/6
1515765056569	C	D	R	/voice/1/6
1515765057563	C	I	S	/voice/1/7
1515765057569	C	D	R	/voice/1/7
1515765058563	C	I	S	/voice/1/8
1515765058569	C	D	R	/voice/1/8
```

