#ifndef PUSH_CONSUMER_HPP
#define PUSH_CONSUMER_HPP

#include <csignal>

// for communication with NFD
#include <ndn-cxx/face.hpp>

// event scheduling
#include <ndn-cxx/util/scheduler.hpp>

// QCI - classes
#include <ndn-cxx/encoding/qci.hpp>

// using boost for cmd_options / file-system handling / timer / shared pointer
#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"

#include "utils.hpp"

//#define DEBUG

using namespace std;
using namespace boost::program_options;
using namespace boost::posix_time;

namespace ndn{

class PushConsumer : noncopyable
{

public:
    PushConsumer(int own_id, string prefix, unsigned int refresh_interval, unsigned int lifetime);
    void run();

protected:
    void onData(const Interest& interest, const Data& data);
    void onTimeout(const Interest& interest);

private:
    boost::asio::io_service m_ioService;
    Face m_face;
    ndn::util::Scheduler m_scheduler;

    std::string m_own_id;
    string m_prefix;
    unsigned int m_refresh_interval;
    unsigned int m_lifetime;

    void expressInterest();
};
}

#endif // PUSH_CONSUMER_HPP
