#ifndef PUSHPRODUCER_H
#define PUSHPRODUCER_H

#include <csignal>

// for communication with NFD
#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>

// event scheduling
#include <ndn-cxx/util/scheduler.hpp>

// using boost for cmd_options / file-system handling / shared pointer
#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"

#include "boost/date_time/posix_time/posix_time.hpp"
#include "utils.hpp"

//#define DEBUG

using namespace std;
using namespace boost::program_options;
using namespace boost::posix_time;

namespace ndn {
class PushProducer : noncopyable
{
public:
    PushProducer(int own_id, string prefix, int data_size, double rate, int freshness_time);
    void run();

protected:

private:
    ndn::security::KeyChain m_keyChain;

    uint32_t m_seq = 0;
    string m_dummyContent;

    bool m_producing = false;

    boost::asio::io_service m_ioService;
    Face m_face;
    ndn::util::Scheduler m_scheduler;

    string own_id;
    string m_prefix;
    int m_data_size;
    double m_rate;
    int m_freshness_time;
    boost::posix_time::ptime m_interest_timeout;

    void onInterest(const InterestFilter& filter, const Interest& interest);
    void onRegisterFailed(const Name& prefix, const string& reason);

    string generateContent(const int length);
    void sendPacket();
};
}
#endif // PUSHPRODUCER_H
