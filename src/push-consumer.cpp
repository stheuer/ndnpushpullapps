#include "push-consumer.hpp"

ndn::PushConsumer::PushConsumer(int own_id, string prefix, unsigned int refresh_interval, unsigned int lifetime):m_face(m_ioService), m_scheduler(m_ioService){
    m_own_id = "C" + boost::lexical_cast<std::string>(own_id);
    m_prefix = prefix;
    m_refresh_interval = refresh_interval;
    m_lifetime = lifetime;
}

void ndn::PushConsumer::run()
{
  // schedule first Interest packet to be sent immediately
  m_scheduler.scheduleEvent(time::seconds(0), bind(&ndn::PushConsumer::expressInterest, this));

  // redirect stdout to file
  std::string logfilename = m_own_id + ".log";
  if(!std::freopen(logfilename.c_str(), "w", stdout)){
      std::cerr << "could not open logfile " << m_own_id + ".log, exiting ...";
      return;
  }

  std::cout << "Time\tNode\tType\tSend/Receive\tName\n"; // write header to logfile
  m_face.processEvents();
}

void ndn::PushConsumer::onData(const Interest& interest, const Data& data){
  #ifdef DEBUG
  cout << "received Data packet: " << data.getName().toUri() << endl;
  cout << data;
  cout << "Data: " << data.getContent().value() << endl << endl;
  #endif

  // log data-reception
  std::cout << getMillisecondsSinceEpoch() << "\tC\tD\tR\t" << data.getName().toUri() << "\n";
}

void ndn::PushConsumer::onTimeout(const Interest& interest)
{
   #ifdef DEBUG
   cout << "Timeout: " << interest.getName().toUri() << std::endl;
   #endif
}

void ndn::PushConsumer::expressInterest()
{
  // schedule the sending of the next Data packet
  m_scheduler.scheduleEvent(time::milliseconds(m_refresh_interval), bind(&ndn::PushConsumer::expressInterest, this));

  // setup / send Interest packet
  Name interestName(m_prefix);
  Interest interest(interestName);
  interest.setInterestLifetime(time::milliseconds(m_lifetime));
  time::milliseconds creationTime(microsec_clock::local_time().time_of_day().total_milliseconds());
  interest.setNonce(creationTime.count() + (rand() % 1000));
  //interest.setMustBeFresh(true);  // not present in original impl. (ndnSim) of push-consumer
  interest.setPush(true);
  //interest.setQCI(ndn::QCI_CLASSES::QCI_1);  // fixed qci for PIs

  m_face.expressInterest(interest,
                         bind(&ndn::PushConsumer::onData, this, _1, _2),
                         bind(&ndn::PushConsumer::onTimeout, this, _1));

  // log interest-expression
  std::cout << getMillisecondsSinceEpoch() << "\tC\tI\tS\t" << interest.getName().toUri() << "\n";

  #ifdef DEBUG
  cout << "sent Interest: " << interest.getName().toUri() << endl;
  #endif
}

void signalHandler( int signum ) {
   #ifdef DEBUG
   std::cout << "Interrupt signal (" << signum << ") received.\n";
   #endif

   std::fclose(stdout);  // explicitly flush buffer-content and close stream
   exit(signum);
}

// Main entry-point of the program
int main(int argc, char** argv){
  options_description description("Program options");
  description.add_options ()
      ("own-id,i", value<int>()->required(), "Id-number of the instance (Required)")
      ("prefix,p", value<string>()->required (), "Prefix the push-consumer uses to request content (Required)")
      ("refresh-interval,r", value<unsigned int>(), "Interval of interest reissuing (Required, in msec)")
      ("lifetime,l", value<unsigned int>(), "Interest lifetime (Default 1000msec)");

  positional_options_description positionalOptions;
  variables_map vm;

  try {
    // throws on error
    store(command_line_parser(argc, argv).options(description).positional(positionalOptions).run(),vm);

    notify(vm); //notify if required parameters are not provided.
  }
  catch(boost::program_options::required_option& e){ // user forgot to provide a required option
    string appName = boost::filesystem::basename(argv[0]);

    cerr << description << endl;

    cerr << endl;
    cerr << "ERROR: " << e.what() << endl;
    cerr << "usage-example: " << "./" << appName << " --prefix foo --frequency 10" << endl;
    cerr << endl;
    return -1;
  }
  catch(boost::program_options::error& e){ // a given parameter is faulty (e.g. given a string, asked for int)
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    return -1;
  }
  catch(std::exception& e){
    std::cerr << "Unhandled Exception reached the top of main: "
              << e.what() << ", application will now exit" << std::endl;
    return -1;
  }

  //default values
  unsigned int lifetime = (vm.count ("lifetime")) ? vm["lifetime"].as<unsigned int>() : 1000;

  ndn::PushConsumer pushConsumer(vm["own-id"].as<int>(),
                         vm["prefix"].as<std::string>(),
                         vm["refresh-interval"].as<unsigned int>(),
                         lifetime);

  try{
    // register signals indicating (probably) impending execution end
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    pushConsumer.run();
  }
  catch (const std::exception& e){
    std::cerr << "ERROR: " << e.what() << std::endl;
  }

  return 0;
}
