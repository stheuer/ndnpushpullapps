#include <sys/time.h>
#include "utils.hpp"

long getMillisecondsSinceEpoch(){
    struct timeval timepoint;
    gettimeofday(&timepoint, nullptr);
    return timepoint.tv_sec * 1000 + timepoint.tv_usec / 1000;
}
