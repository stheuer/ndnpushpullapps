#include "push-producer.hpp"

ndn::PushProducer::PushProducer(int own_id, string prefix, int data_size, double rate,
                                int freshness_time) : m_face(m_ioService), m_scheduler(m_ioService){

    this->own_id = "S" + boost::lexical_cast<std::string>(own_id);
    m_prefix = prefix;
    m_data_size = data_size;
    m_rate = rate;
    m_freshness_time = freshness_time;

    // create content once, always push the same content (m_seq++) to reduce cpu-load
    m_dummyContent = generateContent(data_size);
}

// register prefix on NFD
void ndn::PushProducer::run()
{
    m_face.setInterestFilter(m_prefix,
                             bind(&ndn::PushProducer::onInterest, this, _1, _2),
                             RegisterPrefixSuccessCallback(),
                             bind(&ndn::PushProducer::onRegisterFailed, this, _1, _2));

    // redirect stdout to file
    std::string logfilename = own_id + ".log";
    if(!std::freopen(logfilename.c_str(), "w", stdout)){
        std::cerr << "could not open logfile " << own_id + ".log, exiting ...";
        return;
    }

    cout << "Time\tNode\tType\tSend/Receive\tName\n"; // write header to logfile
    m_face.processEvents();
}



void ndn::PushProducer::onRegisterFailed(const Name& prefix, const std::string& reason)
{
    std::cerr << "ERROR: Failed to register prefix \""
              << prefix << "\" in local hub's daemon (" << reason << ")"
              << std::endl;
    m_face.shutdown();
}

void ndn::PushProducer::onInterest(const InterestFilter& filter, const Interest& interest)
{
    #ifdef DEBUG
    cout << "Received Interest: " << interest.getName().toUri() << endl;
    #endif
    m_interest_timeout = boost::posix_time::microsec_clock::universal_time() + boost::posix_time::millisec(interest.getInterestLifetime().count());

    // log interest-reception
    cout << getMillisecondsSinceEpoch() << "\tS\tI\tR\t" << interest.getName().toUri() << "\n";

    // If producer already produces data, do nothing
    if (m_producing)
      return;

    #ifdef DEBUG
    cout << "Starting to produce now" << endl;
    #endif

    // schedule first Data-packet to be sent/pushed immediately
    m_producing = true;
    m_scheduler.scheduleEvent(time::seconds(0), bind(&ndn::PushProducer::sendPacket, this));
}

void ndn::PushProducer::sendPacket()
{
  // all interests timed out, stop and wait until a new interest arrives
  if(boost::posix_time::microsec_clock::universal_time() > m_interest_timeout){
      m_producing = false;

      #ifdef DEBUG
      cout << "All interests timed out, no consumers left, stopping production" << endl;
      #endif
      return;
  }

  // schedule the sending of the next Data packet
  m_scheduler.scheduleEvent(time::microseconds((int)(1000000.0/m_rate)), bind(&ndn::PushProducer::sendPacket, this));

  // create Data packet
  shared_ptr<Data> data = make_shared<Data>();

  data->setName(Name(m_prefix + "/" + boost::lexical_cast<std::string>(m_seq++)));
  data->setPush(true);
  data->setFreshnessPeriod(time::seconds(m_freshness_time));
  data->setContent(reinterpret_cast<const uint8_t*>(m_dummyContent.c_str()), m_dummyContent.size());

  // Sign Data packet with default identity
  m_keyChain.sign(*data);

  #ifdef DEBUG
  cout << "sending Data packet: " << data->getName().toUri() << endl;
  #endif

  // log the imminent sending of the Data packet
  cout << getMillisecondsSinceEpoch() << "\tS\tD\tS\t" << data->getName().toUri() << "\n";

  // send Data packet
  m_face.put(*data);
}

// returns a std::string of a given length, consisting of random alphanumeric characters
string ndn::PushProducer::generateContent(const int length)
{
  static const char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char content[length];

  for (int i = 0; i < length; ++i)
    content[i] = alphabet[i % (sizeof(alphabet) - 1)];
  return string(content);
}

void signalHandler( int signum ) {
   #ifdef DEBUG
   cout << "Interrupt signal (" << signum << ") received.\n";
   #endif

   std::fclose(stdout);  // explicitly flush buffer-content and close stream
   exit(signum);
}

// Main entry-point of the program
int main(int argc, char** argv){
    options_description description("Program options");
    description.add_options()
        ("own-id,i", value<int>()->required(), "Id-number of the instance (Required)")
        ("prefix,p", value<string>()->required(), "Prefix the push-producer listens too. (Required)")
        ("data-size,s", value<int>()->required(), "The size of a data-packet in bytes. (Required)")
        ("rate,r", value<double>()->required(), "The number of pushed data-packets per second (Required)")
        ("freshness-time,t", value<int>()->default_value(300), "Freshness time of the content in seconds. (Default 5min)");

    positional_options_description positionalOptions;
    variables_map vm;

    try
    {
      // throws on error
      store(command_line_parser(argc, argv).options(description).positional(positionalOptions).run(),vm);
      notify(vm); //notify if required parameters are not provided.
    }
    catch(boost::program_options::required_option& e){ // user forgot to provide a required option
      std::string appName = boost::filesystem::basename(argv[0]);

      cerr << description << endl;

      cerr << endl;
      cerr << "ERROR: " << e.what() << endl;
      cerr << "usage-example: " << "./" << appName << " --prefix foo --data-size 1024 --frequency 10" << endl;
      cerr << endl;
      return -1;
    }
    catch(boost::program_options::error& e){ // a given parameter is faulty (e.g. given a string, asked for int)
      cerr << "ERROR: " << e.what() << endl << endl;
      return -1;
    }
    catch(exception& e){
      cerr << "Unhandled Exception reached the top of main: "
                << e.what() << ", application will now exit" << endl;
      return -1;
    }

    // default values
    int freshnessTime = (vm.count("freshness_time"))? vm["freshness_time"].as<int>() : 300;

    // create new push-producer instance with given parameters
    ndn::PushProducer pushProducer(vm["own-id"].as<int>(),
                                   vm["prefix"].as<string>(),
                                   vm["data-size"].as<int>(),
                                   vm["rate"].as<double>(),
                                   freshnessTime);

    try{
      // register signals indicating (probably) impending execution end
      signal(SIGINT, signalHandler);
      signal(SIGTERM, signalHandler);
      pushProducer.run(); // start producer
    }
    catch (const exception& e){
      cerr << "ERROR: " << e.what() << endl; // report exception(s) if any
      return -1;
    }

    return 0;
}
