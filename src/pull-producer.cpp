// original version: https://github.com/danposch/ndn-apps
#include <iostream>
#include <csignal>
#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>

#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"
#include "utils.hpp"

//#define DEBUG
using namespace boost::program_options;

namespace ndn
{

class PullProducer : noncopyable
{
public:

  PullProducer(int own_id, std::string prefix, int data_size, int fresshness_seconds)
  {
    this->own_id = "S" + boost::lexical_cast<std::string>(own_id);
    this->prefix = prefix;
    this->data_size = data_size;
    this->fresshness_seconds = fresshness_seconds;
    this->dummyContent = generateContent(data_size);
  }

  void run()
  {
    m_face.setInterestFilter(this->prefix,
                             bind(&PullProducer::onInterest, this, _1, _2),
                             RegisterPrefixSuccessCallback(),
                             bind(&PullProducer::onRegisterFailed, this, _1, _2));

    // redirect stdout to file
    std::string logfilename = own_id + ".log";
    if(!std::freopen(logfilename.c_str(), "w", stdout)){
        std::cerr << "could not open logfile " << own_id + ".log, exiting ...";
        return;
    }

    std::cout << "Time\tNode\tType\tSend/Receive\tName\n"; // write header to logfile
    m_face.processEvents();
  }

private:
  std::string generateContent(const int length)
  {
    static const char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char content[length];

    for (int i = 0; i < length; ++i)
      content[i] = alphabet[i % (sizeof(alphabet) - 1)];
    return std::string(content);
  }

  void onInterest(const InterestFilter& filter, const Interest& interest)
  {
    #ifdef DEBUG
    std::cout << "Received Interest: " << interest << std::endl;
    #endif

    // log interest-reception
    std::cout << getMillisecondsSinceEpoch() << "\tS\tI\tR\t" << interest.getName().toUri() << "\n";

    // Create new name, based on Interest's name
    Name dataName(interest.getName());

    // Create Data packet
    shared_ptr<Data> data = make_shared<Data>();
    data->setName(dataName);
    data->setFreshnessPeriod(time::seconds(fresshness_seconds));
    data->setContent(reinterpret_cast<const uint8_t*>(dummyContent.c_str()), dummyContent.size());

    // Sign Data packet with default identity
    m_keyChain.sign(*data);

    // log the imminent sending of the Data packet
    std::cout << getMillisecondsSinceEpoch() << "\tS\tD\tS\t" << data->getName().toUri() << "\n";

    // Return Data packet
    m_face.put(*data);
  }

  void onRegisterFailed(const Name& prefix, const std::string& reason)
  {
    std::cerr << "ERROR: Failed to register prefix \""
              << prefix << "\" in local hub's daemon (" << reason << ")"
              << std::endl;
    m_face.shutdown();
  }

private:
  Face m_face;
  KeyChain m_keyChain;
  int data_size;
  int fresshness_seconds;
  std::string own_id;
  std::string prefix;
  std::string dummyContent;
};

} // namespace ndn

void signalHandler( int signum ) {
   #ifdef DEBUG
   std::cout << "Interrupt signal (" << signum << ") received.\n";
   #endif

   std::fclose(stdout);  // explicitly flush buffer-content and close stream
   exit(signum);
}

int main(int argc, char** argv)
{
  options_description description("Programm Options");
  description.add_options ()
      ("own-id,i", value<int>()->required(), "Id-number of the instance (Required)")
      ("prefix,p", value<std::string>()->required (), "Prefix the PullProducer listens too. (Required)")
      ("data-size,s", value<int>()->required (), "The size of the datapacket in bytes. (Required)")
      ("freshness-time,f", value<int>(), "Freshness time of the content in seconds. (Default 5min)");

  positional_options_description positionalOptions;
  variables_map vm;

  try
  {
    // throws on error
    store(command_line_parser(argc, argv).options(description).positional(positionalOptions).run(),vm);
    notify(vm); //notify if required parameters are not provided.
  }
  catch(boost::program_options::required_option& e)
  {
      std::string appName = boost::filesystem::basename(argv[0]);

      std::cerr << description << std::endl;

      std::cerr << std::endl;
      std::cerr << "ERROR: " << e.what() << std::endl;
      std::cerr << "usage-example: " << "./" << appName << " --prefix /foo --data-size 80" << std::endl;
      std::cerr << std::endl;
      return -1;
  }
  catch(boost::program_options::error& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    return -1;
  }
  catch(std::exception& e)
  {
    std::cerr << "Unhandled Exception reached the top of main: "
              << e.what() << ", application will now exit" << std::endl;
    return -1;
  }

  int freshness_time = 300;
  if(vm.count ("freshness-time"))
  {
    freshness_time = vm["freshness-time"].as<int>();
  }

  ndn::PullProducer producer(vm["own-id"].as<int>(),
                         vm["prefix"].as<std::string>(),
                         vm["data-size"].as<int>(),
                         freshness_time);

  try
  {
    // register signals indicating (probably) impending execution end
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    producer.run();
  }
  catch (const std::exception& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl;
  }

  return 0;
}
