// original version: https://github.com/danposch/ndn-apps
#include <iostream>
#include <csignal>
#include <ndn-cxx/face.hpp>
#include <ndn-cxx/util/scheduler.hpp>

#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/asio/deadline_timer.hpp"
#include "utils.hpp"

//#define DEBUG
using namespace boost::program_options;

namespace ndn {

class PullConsumer : noncopyable
{
public:

  PullConsumer(int own_id, std::string prefix, int rate, int i_lifetime) : m_face(m_ioService), m_scheduler(m_ioService)
  {
    this->own_id = "C" + boost::lexical_cast<std::string>(own_id);
    this->prefix = prefix;
    this->rate = rate;
    this->counter = 0;
    this->lifetime = i_lifetime;
  }

  void run()
  {
    boost::asio::deadline_timer timer(m_ioService, boost::posix_time::microseconds(1000000/rate));
    timer.async_wait(bind(&PullConsumer::expressInterest, this, &timer));

    // redirect stdout to file
    std::string logfilename = own_id + ".log";
    if(!std::freopen(logfilename.c_str(), "w", stdout)){
        std::cerr << "could not open logfile " << own_id + ".log, exiting ...";
        return;
    }

    std::cout << "Time\tNode\tType\tSend/Receive\tName\n"; // write header to logfile
    m_face.processEvents();
  }

private:
  void expressInterest(boost::asio::deadline_timer* timer)
  {

    Interest interest(Name(prefix + "/" + boost::lexical_cast<std::string>(counter++)));
    interest.setInterestLifetime(time::milliseconds(lifetime));
    interest.setMustBeFresh(true);

    m_face.expressInterest(interest,
                             bind(&PullConsumer::onData, this,  _1, _2),
                             bind(&PullConsumer::onTimeout, this, _1));

    // log interest-expression
    std::cout << getMillisecondsSinceEpoch() << "\tC\tI\tS\t" << interest.getName().toUri() << "\n";

    #ifdef DEBUG
    std::cout << "Sending: " << interest << std::endl;
    #endif

    timer->expires_at (timer->expires_at () + boost::posix_time::microseconds(1000000/rate));
    timer->async_wait(bind(&PullConsumer::expressInterest, this, timer));
  }

  void onData(const Interest& interest, const Data& data)
  {
    #ifdef DEBUG
      std::cout << "received Data packet: " << data.getName().toUri() << std::endl;
      std::cout << data;
      std::cout << "Data: " << data.getContent().value() << std::endl << std::endl;
    #endif

    // log data-reception
    std::cout << getMillisecondsSinceEpoch() << "\tC\tD\tR\t" << data.getName().toUri() << "\n";
  }

  void onTimeout(const Interest& interest)
  {
    #ifdef DEBUG
    std::cout << "Timeout " << interest << std::endl;
    #endif
  }

private:
  boost::asio::io_service m_ioService;
  Face m_face;
  Scheduler m_scheduler;
  std::string own_id;
  std::string prefix;
  int rate;
  int counter;
  int lifetime;
};

}

void signalHandler( int signum ) {
   #ifdef DEBUG
   std::cout << "Interrupt signal (" << signum << ") received.\n";
   #endif

   std::fclose(stdout);  // explicitly flush buffer-content and close stream
   exit(signum);
}

int main(int argc, char** argv) {
  options_description description("Program options");
  description.add_options()
    ("own-id,i", value<int>()->required(), "Id-number of the instance (Required)")
    ("prefix,p", value<std::string>()->required (), "Prefix the PullConsumer uses to request content. (Required)")
    ("rate,r", value<int>()->required (), "Interests per second issued. (Required)")
    ("lifetime,l", value<int>(), "Interest Lifetime (Default 1000msec)");

  positional_options_description positionalOptions;
  variables_map vm;

  try
  {
    // throws on error
    store(command_line_parser(argc, argv).options(description).positional(positionalOptions).run(),vm);
    notify(vm); //notify if required parameters are not provided.
  }
  catch(boost::program_options::required_option& e){ // user forgot to provide a required option
    std::string appName = boost::filesystem::basename(argv[0]);

    std::cerr << description << std::endl;

    std::cerr << std::endl;
    std::cerr << "ERROR: " << e.what() << std::endl;
    std::cerr << "usage-example: " << "./" << appName << " --prefix /foo --rate 10 --run-time 10" << std::endl;
    std::cerr << std::endl;
    return -1;
  }
  catch(boost::program_options::error& e){ // a given parameter is faulty (e.g. given a string, asked for int)
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    return -1;
  }
  catch(std::exception& e){
    std::cerr << "Unhandled Exception reached the top of main: "
              << e.what() << ", application will now exit" << std::endl;
    return -1;
  }

  int lifetime = 1000;
  if(vm.count ("lifetime"))
  {
    lifetime = vm["lifetime"].as<int>();
  }

  ndn::PullConsumer consumer(vm["own-id"].as<int>(),
                         vm["prefix"].as<std::string>(),
                         vm["rate"].as<int>(),
                         lifetime);

  try
  {
    // register signals indicating (probably) impending execution end
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    consumer.run();
  }
  catch (const std::exception& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl;
  }

  return 0;
}
